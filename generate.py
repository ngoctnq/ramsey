# PARSING INPUT ARGUMENTS
from argparse import ArgumentParser
parser = ArgumentParser(
    description='Generate a graph with a desired Ramsey(4,6,36) value.')
parser.add_argument('-l', '--load-checkpoint', required=True,
    help='The model checkpoint file to be loaded.')
parser.add_argument('-d', '--data', required=True,
    help='The desired Ramsey values for generated graphs.')
parser.add_argument('-o', '--output', required=True,
    help='The file to save results.')
parser.add_argument('-e', '--max-epoch', type=int, default=10 ** 5,
    help='Maximum epochs to be run. '
    'Increase this value for the code to try longer. '
    'Default is 10,000.')
parser.add_argument('-b', '--batch-size', type=int, default=8192,
    help='Batch size for evaluation. '
    'Reduce if you experience out-of-memory issues, but the higher the faster. '
    'Default is 8192.')
args = parser.parse_args()

# IMPORTING NECESSARY PACKAGES
print('[+] Importing necessary packages...', end=' ', flush=True)

import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.optimizers import Adam
from keras.callbacks import Callback, LearningRateScheduler, EarlyStopping
from keras import backend as K

print('Done.')

# LOAD EVALUATION DATA
print('[+] Loading Ramsey values...', end=' ', flush=True)

Y = np.array([int(y) for y in open(args.data).read().strip().split()])
if len(Y) == 0:
    raise RuntimeError('No Ramsey values available!')

print(Y.dtype)
print('Done.')

# LOAD THE MODEL
print('[+] Loading model from checkpoint...')
model = load_model(args.load_checkpoint)
# freeze the trained model
model.trainable = False
# sigmoid activation ensures input values are between 0 and 1
pseudolayer = Dense(324, use_bias=False, activation='sigmoid',
                    input_shape=(len(Y),), name='graph')
pseudolayer.trainable = True
# and add the pseudolayer containing our desired graphs
pseudomodel = Sequential([pseudolayer, model])
print('Done.')

# DEFINE CUSTOM EARLY STOPPING
def real_acc(y_true, y_pred):
    return K.sum(K.cast(K.round(y_pred) == y_true, "int32")) / len(y_pred)

class EndAtFit(Callback):
    def on_epoch_end(self, _, logs=None):
        if logs["real_acc"] == 1:
            print('All graphs found!')
            self.model.stop_training = True

            # SAVE OUTPUT
            print(f'[+] Saving output to {args.output}...', end=' ', flush=True)
            with open(args.output, 'w') as f:
                for row in (pseudolayer.weights[0] > 0).numpy().astype(int):
                    f.write(f'{hex(int("".join(map(str, row)), 2))}\n')
            print('Done.')

            # denote that solution is found
            global found
            found = True

# GRADIENT DESCEND TO THE SOLUTION
X = np.eye(len(Y))
# default parameters for optimizer, specifically lr=0.001
optimizer = Adam(lr=0.1)

def scheduler(epoch, lr):
    # print out current epoch once every 1000
    # comment out the next 2 lines for silence
    if (epoch + 1) % 1000 == 0:
        print(f'Epoch {epoch}...')
    # epochs using initial LR
    if epoch > 512:
        epoch_ = bin(epoch)[3:]
        # epoch is power of 2, minimum LR, reduce only every 2 powers
        if epoch_.count('0') == 0 and lr >= 1e-4 and len(epoch_) % 2 == 0:
            lr /= 10
            # print every time LR changes, comment out for silence
            print(f'Epoch {epoch}: LR dropped down to {lr:.5f}')
    return lr

callbacks = [
    LearningRateScheduler(scheduler),
    EndAtFit()
]

found = False
pseudomodel.compile(optimizer=optimizer, loss='mse', metrics=[real_acc],)
# run for a maximum of 10,000 epochs,
# if one wants to wait longer, increase this 10 ** 5 value
pseudomodel.fit(X, Y, epochs=args.max_epoch, verbose=0,
                callbacks=callbacks,
                batch_size=args.batch_size)

if not found:
    idxs = (K.round(pseudomodel(X)) == Y).numpy().astype(bool).flatten()
    print(f"Failed to find all graphs, only found {np.sum(idxs)}/{len(Y)}.")
    
    # ONLY WRITE OUT IF SOME GRAPHS ARE FOUND
    if np.sum(idxs) > 0:
        print("Failed graphs will not be saved, instead will have '-' in the output file.")

        # SAVE ONLY THE FOUND GRAPHS
        print(f'[+] Saving found graphs to {args.output}...', end=' ', flush=True)
        with open(args.output, 'w') as f:
            for row, found_ in zip((pseudolayer.weights[0] > 0).numpy().astype(int), idxs):
                if found_:
                    f.write(f'{hex(int("".join(map(str, row)), 2))}\n')
                else:
                    f.write('-\n')
        print('Done.')