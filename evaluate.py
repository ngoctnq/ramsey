# PARSING INPUT ARGUMENTS
from argparse import ArgumentParser
parser = ArgumentParser(
    description='Evaluate a model to estimate Ramsey(4,6,36) value of a batch of graphs.')
parser.add_argument('-l', '--load-checkpoint', required=True,
    help='The model checkpoint file to be loaded.')
parser.add_argument('-d', '--data', required=True,
    help='The graph to be evaluated.')
parser.add_argument('-o', '--output', required=True,
    help='The file to save results.')
parser.add_argument('-b', '--batch-size', type=int, default=8192,
    help='Batch size for evaluation. '
    'Reduce if you experience out-of-memory issues, but the higher the faster. '
    'Default is 8192.')
args = parser.parse_args()

# IMPORTING NECESSARY PACKAGES
print('[+] Importing necessary packages...', end=' ', flush=True)

import numpy as np
from keras.models import load_model

print('Done.')

# LOAD EVALUATION DATA
print('[+] Loading graph data...', end=' ', flush=True)

X = []
for line in open(args.data):
    # this will work even if data is formatted like training (i.e. with label)
    x = line.strip().split()[0]
    X.append([int(i) for i in f'{int(x, 16):0324b}'])

# if there is no data, exit
if len(X) == 0:
    raise RuntimeError('No graph data available!')

X = np.array(X)

print('Done.')

# LOAD THE MODEL
print('[+] Loading model from checkpoint...')
model = load_model(args.load_checkpoint)
print('Done.')

# EVALUATE ON THE DATA
Y = np.round(model.predict(X)).astype(np.int).flatten()

# SAVE OUTPUT
print(f'[+] Saving output to {args.output}...', end=' ', flush=True)
with open(args.output, 'w') as f:
    for y in Y:
        f.write(f'{y}\n')
print('Done.')
