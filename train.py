###########################
# PARSING INPUT ARGUMENTS #
###########################

from argparse import ArgumentParser
parser = ArgumentParser(
    description='Train a model to estimate Ramsey(4,6,36) value of a graph.')
parser.add_argument('-l', '--load-checkpoint',
    help='The model checkpoint file to be used as a starting point, '
    'useful for fine-tuning an existing model with new data. '
    'Default is to train from scratch.')
parser.add_argument('-s', '--save-checkpoint',
    help='The location to save the newly trained model. '
    'Default is "checkpoints/ramsey_<time_of_save>.h5"')
parser.add_argument('-b', '--batch-size', type=int, default=8192,
    help='Batch size for training. '
    'Reduce if you experience out-of-memory issues, but the higher the better. '
    'Default is 8192.')
args = parser.parse_args()

################################
# IMPORTING NECESSARY PACKAGES #
################################

print('[+] Importing necessary packages...', end=' ', flush=True)

from datetime import datetime
from glob import glob
import numpy as np
from keras.layers import Dense
from keras.models import Sequential, load_model
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ReduceLROnPlateau

print('Done.')

######################
# LOAD TRAINING DATA #
######################

print('[+] Loading training data...', end=' ', flush=True)

# get all the file names
fnames = glob('data/*')
X, Y = [], []
# loop through the files and read data
for fname in fnames:
    for line in open(fname):
        x, y = line.split()
        X.append([int(i) for i in f'{int(x, 16):0324b}'])
        Y.append(int(y))

# if there is no data, exit
if len(X) == 0:
    raise RuntimeError('No training data available!')

X = np.array(X)
Y = np.array(Y)

print('Done.')

###################
# BUILD THE MODEL #
###################


# if no checkpoint is specified, build a new model
if args.load_checkpoint is None:
    print('[+] No checkpoint specified, creating model from scratch.')
    model = Sequential([
        Dense(648, activation='tanh', input_shape=(324,)),
        Dense(1296, activation='tanh'),
        Dense(2592, activation='tanh'),
        Dense(5184, activation='tanh'),
        Dense(2592, activation='sigmoid'),
        Dense(1296, activation='sigmoid'),
        Dense(648, activation='sigmoid'),
        Dense(1)
    ])
# else load existing model
else:
    print('[+] Loading model from checkpoint...')
    model = load_model(args.load_checkpoint)

########################
# START TRAINING MODEL #
########################

# default parameters for optimizer, specifically lr=0.001
optimizer = Adam()
callbacks = [
    # stop the training if the validation loss does not decrease after 50 epochs
    EarlyStopping(patience=50, restore_best_weights=True),
    # reduce the learning rate if validation loss does not decrease after 25 epochs
    ReduceLROnPlateau(patience=25)
]

model.compile(optimizer=optimizer, loss='mse')
model.fit(X, Y, epochs=1000, callbacks=callbacks,
          batch_size=args.batch_size, validation_split=0.15)

#####################
# SAVE OUTPUT MODEL #
#####################

if args.save_checkpoint is None:
    time = str(datetime.now()).replace(' ', 'T').split('.')[0]
    args.save_checkpoint = f'checkpoints/ramsey_{time}.h5'
print(f'[+] Saving trained model to {args.save_checkpoint}...', end=' ', flush=True)
model.save(args.save_checkpoint)
print('Done.')
